## **Pocket Simulator! by:**
 - Timothy Sayson
-  Ryan Tran
-  Klein Del Campo
-  Gary Le

## Design information
- [Wire-frame diagrams](https://www.figma.com/file/HBhyuVOXfETmouuvMZXQHj/Wireframes?type=design&node-id=0-1&mode=design&t=EHWh4CcPnmChPaYU-0)
- [APIs](https://gitlab.com/team-10-everyday/module3-project-gamma/-/blob/main/docs/APIs.md?ref_type=heads)
- [Data Tables](https://gitlab.com/team-10-everyday/module3-project-gamma/-/blob/main/docs/Datatables.md?ref_type=heads)
- [Journals](https://gitlab.com/team-10-everyday/module3-project-gamma/-/tree/main/journals?ref_type=heads)

## Functionality
- Pocket Simulator was designed so you could battle using pokemon you own in whichever version you are playing!
- Users can create an account and create teams for themselves
- Users then select their teams they created, and get randomly queue'd up with an opponent.
- The system selects a winner based on type matchups.
- System autmatically stores all of your duel history.
- Access to list all types with strengths / weaknesses.
- Able to see all Pokemon currently registered.

## Where we're headed
- Want to implement third-party API to have all the Pokemon available, and not just hard coded ones.
- Add a friends list / chat feature.
- Want to go more in depth with stat lines and make pokemon more unique.

## Implementation
To get our app onto your local machine:

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Run docker volume create example-data
4. Run docker compose build 
5. Run docker compose up 
6. Run docker exec -it module3-project-gamma-fastapi-1 bash
7. Run python -m migrations up
8. Exit and head to http://localhost:3000
