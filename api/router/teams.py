from fastapi import (APIRouter,
                     Depends
                     )
from typing import Optional, Union
from query.teams import (Error,
                         TeamIn,
                         TeamOut,
                         TeamRepository)
from authenticator import authenticator

router = APIRouter()


@router.post("/teams", response_model=Union[TeamOut, Error])
def create_team(
    team: TeamIn,
    repo: TeamRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    owner_id = account["id"]
    return repo.create_team(team, owner_id)


@router.put("/teams/{team_id}",
            response_model=Optional[TeamIn])
def update_team(
        team_id: int,
        team: TeamIn,
        repo: TeamRepository = Depends()
) -> Optional[TeamIn]:
    return repo.update_team(team_id, team)


@router.delete("/teams/{team_id}", response_model=bool)
def delete_team(
    team_id: int,
    repo: TeamRepository = Depends(),
) -> bool:
    return repo.delete(team_id)
