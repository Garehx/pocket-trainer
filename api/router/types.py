from fastapi import APIRouter, Depends
from typing import List, Optional, Union
from query.types import (Error, TypeOut, TypeRepository)

router = APIRouter()


@router.get("/types", response_model=Union[List[TypeOut], Error])
def get_types(
        repo: TypeRepository = Depends()
):
    return repo.get_types()


@router.get("/types/{type_id}", response_model=Optional[TypeOut])
def get_type(
    type_id: int,
    repo: TypeRepository = Depends(),
) -> Optional[TypeOut]:
    type = repo.get_type(type_id)
    return type
