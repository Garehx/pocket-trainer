from fastapi import APIRouter, Depends, Request
from typing import Optional, Union
from query.duels import Error, DuelIn, DuelOut, DuelCreateOut, DuelRepository
from authenticator import authenticator

router = APIRouter()


@router.get("/api/protected", response_model=bool)
async def get_token(request: Request,
                    account_data: dict = Depends(
                        authenticator.get_current_account_data)):
    return True


@router.post("/duels", response_model=Union[DuelCreateOut, Error])
def create_duel(
    duel: DuelIn,
    repo: DuelRepository = Depends(),
):
    return repo.create_duel(duel)


@router.get("/duels/{duel_id}", response_model=Optional[DuelOut])
def get_duel(
        duel_id: int,
        repo: DuelRepository = Depends(),
) -> Optional[DuelOut]:
    duel = repo.get_duel(duel_id)
    return duel
