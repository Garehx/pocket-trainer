from fastapi.testclient import TestClient
from main import app
from query.types import TypeRepository

client = TestClient(app)


class EmptyTypeRepository:

    def get_types(self):
        return [
            {
              "id": 1,
              "image":
              "https://upload.wikimedia.org/wikipedia.svg.png",
              "typename": "Electric",
              "strengths": "Water",
              "weaknesses": "Rock"
            },
            {
              "id": 2,
              "image": "https://image.pngaaa.com/886/6175886-middle.png",
              "typename": "Fire",
              "strengths": "Grass",
              "weaknesses": "Water"
            },
            {
              "id": 3,
              "image":
              "https://upload.wikimedia.org/wikipedia.svg.png",
              "typename": "Water",
              "strengths": "Fire",
              "weaknesses": "Electric"
            }
        ]


def test_get_types():
    app.dependency_overrides[TypeRepository] = EmptyTypeRepository

    response = client.get("/types")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == [
             {
              "id": 1,
              "image":
              "https://upload.wikimedia.org/wikipedia.svg.png",
              "typename": "Electric",
              "strengths": "Water",
              "weaknesses": "Rock"
              },
             {
              "id": 2,
              "image": "https://image.pngaaa.com/886/6175886-middle.png",
              "typename": "Fire",
              "strengths": "Grass",
              "weaknesses": "Water"
              },
             {
              "id": 3,
              "image":
              "https://upload.wikimedia.org/wikipedia.svg.png",
              "typename": "Water",
              "strengths": "Fire",
              "weaknesses": "Electric"
              }
        ]
