steps = [
    [

        """
        CREATE TABLE enemies (
            id serial not null primary key,
            img varchar(1000),
            enemyname varchar(100) not null unique
        );

        INSERT INTO enemies (img, enemyname) VALUES ('https://archives.bulbagarden.net/media/upload/6/63/ORAS_Aroma_Lady.png', 'Flora Bloomfield');
        INSERT INTO enemies (img, enemyname) VALUES ('https://archives.bulbagarden.net/media/upload/8/81/XY_Black_Belt.png', 'Bruce Chan');
        """,



        """
        DROP TABLE enemies;
        """


    ],

    [
        
        """
        CREATE TABLE enemyteams (
            id serial not null primary key,
            enemyteamownerid int not null,
            pokemon1 int
        );

        INSERT INTO enemyteams (enemyteamownerid, pokemon1) VALUES (1, 4);
        INSERT INTO enemyteams (enemyteamownerid, pokemon1) VALUES (2, 6);
        """,



        """
        DROP TABLE enemyteams;
        """


    ],


]
