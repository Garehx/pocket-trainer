from pydantic import BaseModel
from query.pool import pool
from typing import Union


class Error(BaseModel):
    message: str


class OwnedPokemonIn(BaseModel):
    owner_id: int
    pokemon_id: int


class OwnedPokemonOut(BaseModel):
    id: int
    owner_id: int
    pokemon_id: int


class OwnedPokemonRepository:
    def create_owned_pokemon(self,
                             owned_pokemon: OwnedPokemonIn) -> Union[
                            OwnedPokemonOut,
                            Error]:
        try:

            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO user_pokemon
                            (owner_id, pokemon_id)
                        VALUES
                            (%s, %s)
                         RETURNING id;
                        """,
                        [
                            owned_pokemon.owner_id,
                            owned_pokemon.pokemon_id,
                        ]
                    )
                    id = result.fetchone()[0]

                    return self.owned_pokemon_in_to_out(id, owned_pokemon)
        except Exception as e:
            print(e)
            return {"message": "Create owned_pokemon did not work"}

    def owned_pokemon_in_to_out(self, id: int, owned_pokemon: OwnedPokemonIn):
        old_data = owned_pokemon.dict()
        return OwnedPokemonOut(id=id, **old_data)

    def record_to_owned_pokemon_out(self, record):
        return OwnedPokemonOut(
            id=record[0],
            owner_id=record[1],
            pokemon_id=record[2],
        )
