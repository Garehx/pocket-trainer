import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

function IndividualDuel() {
  const [duel, setDuel] = useState({});
  const { id } = useParams()

  const formatDate = (dateString) => {
    const originalDate = new Date(dateString);
    const formattedDate = `${originalDate.getMonth() + 1}/${originalDate.getDate()}/${originalDate.getFullYear()}`;
    return formattedDate;
  };


  useEffect(() => {
    const getData = async () => {
      const response = await fetch(`${process.env.REACT_APP_API_HOST}/duels/${id}`);
      if (response.ok) {
        const data = await response.json();
        setDuel({ ...data });
      }
    }
    getData();
  }, [id]);


  return (
    <div className="row-form">
      <div className="offset-3 col-6">
        <div className="shadow bg-white p-4 mt-4">
          <div className="duel-header">
            <div className="header-right">
              <h6>Duel Id: {duel.id}</h6><h6>{formatDate(duel.date)}</h6>
            </div>
            <div className="winner">
              <h1 style={{ color: duel.winner === 'User' ? 'green' : 'red' }}>{duel.winner === 'User' ? 'W' : 'L'}</h1>
              <h5>Team {duel.team}</h5>
            </div>
          </div>
          <div className="stats-row">
            <div className="user-team-row">
              <h4>{duel.username}</h4>
              {duel?.user_pokemon?.picture &&
                <h5><img src={duel?.user_pokemon?.picture} className="list-picture" alt={duel?.user_pokemon?.picture} /> {duel?.user_pokemon?.name}</h5>}
            </div>
            <div className="enemy-team-row">
              <h4>{duel.enemyname}</h4>
              {duel?.enemy_pokemon?.picture &&
                <h5><img src={duel.enemy_pokemon.picture} className="list-picture" alt={duel?.enemy_pokemon?.picture} /> {duel?.enemy_pokemon?.name}</h5>}
            </div>
          </div>
        </div>
      </div>
    </div>
  );

}

export default IndividualDuel;
