import React, { useEffect, useState } from 'react';
import './App.css';
import { useNavigate } from "react-router-dom";


function ListTypes() {
  const [type, setType] = useState([]);
  const navigate = useNavigate();

  const getData = async () => {
    const response = await fetch(`${process.env.REACT_APP_API_HOST}/types`);
    if (response.ok) {
      const data = await response.json();
      setType(data);
    }
  }

  const goToType = (id) => {
    navigate(`/types/${id}`);
  }

  useEffect(() => {
    getData();
  }, []);


  return (
    <div className="row-form">
      <div className="offset-3 col-6">
        <div className="shadow bg-white p-4 mt-4">
          <table className="table">
            <thead>
              <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Strengths</th>
                <th>Weaknesses</th>
              </tr>
            </thead>
            <tbody>
              {type.map((type) => (
                <tr className="type-row" key={type.id} onClick={() => goToType(type.id)} >
                  <td><img src={type.image} alt={type.typename} className="list-picture" /></td>
                  <td>{type.typename}</td>
                  <td>{type.strengths}</td>
                  <td>{type.weaknesses}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default ListTypes;
