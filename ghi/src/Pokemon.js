import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";
import './App.css';

function Pokemon() {
  const [pokemon, setPokemon] = useState([]);
  const navigate = useNavigate();

  const goToPokemon = (id) => {
    navigate(`/pokemon/${id}`);
  }


  const getData = async () => {
    const response = await fetch(`${process.env.REACT_APP_API_HOST}/pokemon`);
    if (response.ok) {
      const data = await response.json();
      setPokemon(data);
    }
  }

  useEffect(() => {
    getData();
  }, []);


  return (
    <div className="row-form">
      <div className="offset-3 col-6">
        <div className="shadow bg-white p-4 mt-4">
          <h1>Pokemon!</h1>
          <table className="table">
            <thead>
              <tr>
                <th>Photo</th>
                <th>ID</th>
                <th>Pokemon</th>
                <th>Type</th>
              </tr>
            </thead>
            <tbody>
              {pokemon.map((pokemon) => (
                <tr className="pokemon-row" key={pokemon.id} onClick={() => goToPokemon(pokemon.id)} >
                  <td>
                    <img src={pokemon.picture} className="list-picture" alt={pokemon.name} />
                  </td>
                  <td>{pokemon.id}</td>
                  <td>{pokemon.name}</td>
                  <td>{pokemon.type}</td>
                  <td>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default Pokemon;
