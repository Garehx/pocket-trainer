**User Sign-Up (Create Account)**

- Endpoint: POST **`/signup`**
- Request Body: 
```python
{
  "picture": "string",
  "username": "string",
  "firstname": "string",
  "lastname": "string",
  "email": "string",
  "password": "string"
}
```
​
- Response: The response body integrates authenticator logic to turn the user’s password into a hashed password  for safety, and giving the user a token for access to protected endpoints requiring user authorization

```python
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "id": 0,
    "picture": "string",
    "username": "string",
    "email": "string"
  }
}

​```

- When signing up, a db query is also executed to give the user a random pokemon from the database. This information is inserted in the User Pokemon schema (users.py file in query)
-----------------------------------------------------------------------------------------------------------------------------------


**User Sign-In**
- Endpoint: POST **`/token`**

- Request Body
```python

{
  "username": "string",
  "password": "string",
}

```
​
- Response Body

```python
{
  "access_token": "string",
  "token_type": "Bearer"
}

```
​-----------------------------------------------------------------------------------------------------------------------------------

**User Sign-Out**

- Endpoint: POST /token
- Request Body
N/A
​
- Response Body; can be true or null
boolean 
-----------------------------------------------------------------------------------------------------------------------------------

**User Info**

- Endpoint: **`POST /user/protected`**
- Request Body: N/A. The logic to retrieve user info can be found in the route
- The functionality is integrated with authenticator logic to base results on the token belonging to the logged in user
- Response Body:

```python
{
  "id": "int"
  "picture": "string",
  "username": "string",
  "email": "string"
}
```
-----------------------------------------------------------------------------------------------------------------------------------
**Create Team**

- Endpoint: **`POST /teams`**
    - This is also a protected endpoint as creating a team requires an owner_id. This is retrieved through authenticator logic and functionality by calling on the already logged in user / token for the param.
    - The param is used with the data insertion as a third value.
- Request Body:
    
    ```python
    {
      "teamname": "string",
      "pokemon1": "int"
    }
    ```
    
- Response Body

```python
{
  "id": "int",
  "owner_id": "int",
  "teamname": "string",
  "pokemon1": "int"
}
```
-----------------------------------------------------------------------------------------------------------------------------------
**Update Team:**

- Endpoint: **`PUT /teams/{team_id}`**
- Request Body: an id (int) is used as params to pass in with the request body

```python
{
  "teamname": "string",
  "pokemon1": "int"
}
```

- Response Body

```python
{
  "teamname": "string",
  "pokemon1": "int"
}
```
-----------------------------------------------------------------------------------------------------------------------------------
**Delete Team:**

- Endpoint: **`POST /teams/{team_id}`**
- Request Body: an id (int) is used as params to pass in

```python
N/A
```

- Response Body; can be true or null

```python
boolean 
```
-----------------------------------------------------------------------------------------------------------------------------------
**List View of Current User’s Teams**

Endpoint: GET **`/user/teams/protected`**

Request Body: N/A. 
-The logic to retrieve user info can be found in the route 
-The functionality is integrated with authenticator logic to base results on the token belonging to the logged in user 
```python
Response Body:
[
  {
    "id": "int",
    "owner_id": "int",
    "teamname": "string",
    "pokemon1name": "string",
    "pokemon1picture": "string",
    "pokemon1type": "string"
  }
]
```

**List View of Types**
Endpoint: GET **`/types`**

Request Body: 
N/A
​
Response Body
```python
[
  {
    "id": "int",
    "image": "string",
    "typename": "string",
    "strengths": "string",
    "weaknesses": "string"
  }
]
```
​-----------------------------------------------------------------------------------------------------------------------------------

**Individual Type View:**

Endpoint: GET **`/types/{type_id}`**

Request Body: 
N/A. an id (int) is used to pass as params to query for an individual type

Response Body:
```python
{
  "id": "int",
  "image": "string",
  "typename": "string",
  "strengths": "string",
  "weaknesses": "string"
}
```

-----------------------------------------------------------------------------------------------------------------------------------

**Create Duel**
Endpoint: POST **`/duels`**

Request Body: 

```python
{
  "userid": "int",
  "teamid": "int"
}

```

Response Body:

```python
{
  "id": "int",
  "userid": "int",
  "teamid": "int",
  "enemyid": "int",
  "enemyteamid": "int",
  "winner": "string",
  "date": "date"
}
```

​-----------------------------------------------------------------------------------------------------------------------------------

**List Current User’s Duels View**
- Endpoint: GET **`/user/duels/protected`**

- Request Body: N/A. Authenticator logic and functionality is used as params to query by user id provided by the token / authenticated account


- Response Body: 
```python
[
  {
    "duelid": "int",
    "userid": "int",
    "username": "string",
    "teamname": "string",
    "enemyimg": "string",
    "enemyname": "string",
    "winner": "string",
    "date": "date"
  }
]
```

​-----------------------------------------------------------------------------------------------------------------------------------
**Indvidual Duel View**

- Endpoint: GET **`/duels/{duel_id}`**

- Request Body: N/A. a duel id (int) is used as params to query for the given due;

- Response Body: 
```python
{
  "id": "int",
  "username": "string",
  "team": "string",
  "user_pokemon": {
    "name": "string",
    "type": "string",
    "strengths": "string",
    "weaknesses": "string",
    "picture": "string"
  },
  "enemyname": "string",
  "enemy_pokemon": {
    "name": "string",
    "type": "string",
    "strengths": "string",
    "weaknesses": "string",
    "picture": "string"
  },
  "winner": "string",
  "date": "date"
}
```
-----------------------------------------------------------------------------------------------------------------------------------
**List Pokemon View:**

- Endpoint: **`GET /pokemon`**
- Request Body:
    
    ```python
    N/A
    ```
    

- Response Body

```python
[
  {
    "id": "int",
    "picture": "string",
    "name": "string",
    "type": "string",
    "description": "string"
  }
]
```
-----------------------------------------------------------------------------------------------------------------------------------
**Individual Pokemon View:**

- Endpoint: **`GET /pokemon/{pokemon_id}`**
- Request Body:  N/A. Uses params to query for a pokemon based on a unique ID (int)
- Response Body

```python
{
  "id": "int",
  "picture": "string",
  "name": "string",
  "type": "string",
  "strengths": "string",
  "weaknesses": "string",
  "description": "string"
}
```
-----------------------------------------------------------------------------------------------------------------------------------

**Creating a user owned pokemon**

- Endpoint: POST **`/ownedpokemon`**

- Request Body:

```python
{
  "owner_id": "int",
  "pokemon_id": "int"
}

```


- Response Body:

```python
{
  "id": "int",
  "owner_id": "int",
  "pokemon_id": "int"
}
```
​-----------------------------------------------------------------------------------------------------------------------------------

**List Current User’s Owned Pokemon**

- Endpoint: GET **`/user/pokemon/protected`**

- Request Body: N/A. Authenticator logic and functionality is used as params to fetch by user id provided by the token / authenticated account

- Response Body:
```python

[
  {
    "id": "int",
    "owner_id": "int",
    "pokemon_id": "int",
    "picture": "string",
    "pokemon": "string",
    "type": "string",
    "strength": "string",
    "weaknesses": "string"
  }
]
```
